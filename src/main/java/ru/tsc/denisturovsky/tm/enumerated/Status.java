package ru.tsc.denisturovsky.tm.enumerated;

public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed");

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Status status: values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
