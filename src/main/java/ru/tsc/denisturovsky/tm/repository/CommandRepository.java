package ru.tsc.denisturovsky.tm.repository;

import ru.tsc.denisturovsky.tm.api.repository.ICommandRepository;
import ru.tsc.denisturovsky.tm.constant.TerminalArgument;
import ru.tsc.denisturovsky.tm.constant.TerminalCommand;
import ru.tsc.denisturovsky.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command ABOUT = new Command(
            TerminalCommand.ABOUT, TerminalArgument.ABOUT,
            "Show developer info"
    );

    public static Command INFO = new Command(
            TerminalCommand.INFO, TerminalArgument.INFO,
            "Show system info"
    );

    public static Command HELP = new Command(
            TerminalCommand.HELP, TerminalArgument.HELP,
            "Show terminal commands list"
    );

    public static Command VERSION = new Command(
            TerminalCommand.VERSION, TerminalArgument.VERSION,
            "Show program version"
    );

    public static Command EXIT = new Command(
            TerminalCommand.EXIT, null,
            "Close application"
    );

    public static Command COMMANDS = new Command(
            TerminalCommand.COMMANDS, TerminalArgument.COMMANDS,
            "Show command list"
    );

    public static Command ARGUMENTS = new Command(
            TerminalCommand.ARGUMENTS, TerminalArgument.ARGUMENTS,
            "Show argument list"
    );

    public static Command TASK_CREATE = new Command(
            TerminalCommand.TASK_CREATE, null,
            "Create new task"
    );

    public static Command TASK_LIST = new Command(
            TerminalCommand.TASK_LIST, null,
            "Show task list"
    );

    public static Command TASK_CLEAR = new Command(
            TerminalCommand.TASK_CLEAR, null,
            "Remove all tasks"
    );

    public static Command TASK_REMOVE_BY_ID = new Command(
            TerminalCommand.TASK_REMOVE_BY_ID, null,
            "Remove task by id"
    );

    public static Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalCommand.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index"
    );

    public static Command TASK_SHOW_BY_ID = new Command(
            TerminalCommand.TASK_SHOW_BY_ID, null,
            "Show task by id"
    );

    public static Command TASK_SHOW_BY_INDEX = new Command(
            TerminalCommand.TASK_SHOW_BY_INDEX, null,
            "Show task by index"
    );

    public static Command TASK_UPDATE_BY_ID = new Command(
            TerminalCommand.TASK_UPDATE_BY_ID, null,
            "Update task by id"
    );

    public static Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalCommand.TASK_UPDATE_BY_INDEX, null,
            "Update task by index"
    );

    public static Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalCommand.TASK_CHANGE_STATUS_BY_ID, null,
            "Change task status by id"
    );

    public static Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalCommand.TASK_CHANGE_STATUS_BY_INDEX, null,
            "Change task status by index"
    );

    public static Command TASK_START_BY_ID = new Command(
            TerminalCommand.TASK_START_BY_ID, null,
            "Start task by id"
    );

    public static Command TASK_START_BY_INDEX = new Command(
            TerminalCommand.TASK_START_BY_INDEX, null,
            "Start task by index"
    );

    public static Command TASK_COMPLETE_BY_ID = new Command(
            TerminalCommand.TASK_COMPLETE_BY_ID, null,
            "Complete task by id"
    );

    public static Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalCommand.TASK_COMPLETE_BY_INDEX, null,
            "Complete task by index"
    );

    public static Command PROJECT_CREATE = new Command(
            TerminalCommand.PROJECT_CREATE, null,
            "Create new project"
    );

    public static Command PROJECT_LIST = new Command(
            TerminalCommand.PROJECT_LIST, null,
            "Show project list"
    );

    public static Command PROJECT_CLEAR = new Command(
            TerminalCommand.PROJECT_CLEAR, null,
            "Remove all projects"
    );

    public static Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalCommand.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id"
    );

    public static Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalCommand.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index"
    );

    public static Command PROJECT_SHOW_BY_ID = new Command(
            TerminalCommand.PROJECT_SHOW_BY_ID, null,
            "Show project by id"
    );

    public static Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalCommand.PROJECT_SHOW_BY_INDEX, null,
            "Show project by index"
    );

    public static Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalCommand.PROJECT_UPDATE_BY_ID, null,
            "Update project by id"
    );

    public static Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalCommand.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index"
    );

    public static Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalCommand.PROJECT_CHANGE_STATUS_BY_ID, null,
            "Change project status by id"
    );

    public static Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalCommand.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Change project status by index"
    );

    public static Command PROJECT_START_BY_ID = new Command(
            TerminalCommand.PROJECT_START_BY_ID, null,
            "Start project by id"
    );

    public static Command PROJECT_START_BY_INDEX = new Command(
            TerminalCommand.PROJECT_START_BY_INDEX, null,
            "Start project by index"
    );

    public static Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalCommand.PROJECT_COMPLETE_BY_ID, null,
            "Complete project by id"
    );

    public static Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalCommand.PROJECT_COMPLETE_BY_INDEX, null,
            "Complete project by index"
    );

    private final Command[] terminalCommands = new Command[]{
            ABOUT, INFO, HELP, VERSION,
            COMMANDS, ARGUMENTS,
            TASK_CLEAR, TASK_CREATE, TASK_LIST,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX,
            PROJECT_CLEAR, PROJECT_CREATE, PROJECT_LIST,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}
