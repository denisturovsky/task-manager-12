package ru.tsc.denisturovsky.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showErrorCommand(String command);

    void showErrorArgument(String arg);

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showAbout();

    void showWelcome();

}
