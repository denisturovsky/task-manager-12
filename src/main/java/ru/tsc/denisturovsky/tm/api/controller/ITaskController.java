package ru.tsc.denisturovsky.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void clearTasks();

    void createTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

}
